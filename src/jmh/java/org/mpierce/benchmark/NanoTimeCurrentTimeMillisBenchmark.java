package org.mpierce.benchmark;

import org.openjdk.jmh.annotations.Benchmark;
import org.openjdk.jmh.annotations.BenchmarkMode;
import org.openjdk.jmh.annotations.Mode;

@BenchmarkMode({Mode.Throughput, Mode.SampleTime})
public class NanoTimeCurrentTimeMillisBenchmark {

    @Benchmark
    public long nanoTime() {
        return System.nanoTime();
    }

    @Benchmark
    public long currentTimeMillis() {
        return System.currentTimeMillis();
    }
}
