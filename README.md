Output from java 11.0.1 on Linux 4.14 on a core i7-6850k.

```

REMEMBER: The numbers below are just data. To gain reusable insights, you need to follow up on
why the numbers are the way they are. Use profilers (see -prof, -lprof), design factorial
experiments, perform baseline and negative tests that provide experimental control, make sure
the benchmarking environment is safe on JVM/OS/HW level, ask for reviews from the domain experts.
Do not assume the numbers tell you what you want them to tell.

Benchmark                                                                         Mode      Cnt         Score         Error  Units
NanoTimeCurrentTimeMillisBenchmark.currentTimeMillis                             thrpt       25  53991389.017 ± 1067997.104  ops/s
NanoTimeCurrentTimeMillisBenchmark.nanoTime                                      thrpt       25  54116398.185 ±   45348.246  ops/s
NanoTimeCurrentTimeMillisBenchmark.currentTimeMillis                            sample  5682795        ≈ 10⁻⁷                 s/op
NanoTimeCurrentTimeMillisBenchmark.currentTimeMillis:currentTimeMillis·p0.00    sample                 ≈ 10⁻⁷                 s/op
NanoTimeCurrentTimeMillisBenchmark.currentTimeMillis:currentTimeMillis·p0.50    sample                 ≈ 10⁻⁷                 s/op
NanoTimeCurrentTimeMillisBenchmark.currentTimeMillis:currentTimeMillis·p0.90    sample                 ≈ 10⁻⁷                 s/op
NanoTimeCurrentTimeMillisBenchmark.currentTimeMillis:currentTimeMillis·p0.95    sample                 ≈ 10⁻⁷                 s/op
NanoTimeCurrentTimeMillisBenchmark.currentTimeMillis:currentTimeMillis·p0.99    sample                 ≈ 10⁻⁷                 s/op
NanoTimeCurrentTimeMillisBenchmark.currentTimeMillis:currentTimeMillis·p0.999   sample                 ≈ 10⁻⁷                 s/op
NanoTimeCurrentTimeMillisBenchmark.currentTimeMillis:currentTimeMillis·p0.9999  sample                 ≈ 10⁻⁶                 s/op
NanoTimeCurrentTimeMillisBenchmark.currentTimeMillis:currentTimeMillis·p1.00    sample                 ≈ 10⁻³                 s/op
NanoTimeCurrentTimeMillisBenchmark.nanoTime                                     sample  5954454        ≈ 10⁻⁷                 s/op
NanoTimeCurrentTimeMillisBenchmark.nanoTime:nanoTime·p0.00                      sample                 ≈ 10⁻⁸                 s/op
NanoTimeCurrentTimeMillisBenchmark.nanoTime:nanoTime·p0.50                      sample                 ≈ 10⁻⁷                 s/op
NanoTimeCurrentTimeMillisBenchmark.nanoTime:nanoTime·p0.90                      sample                 ≈ 10⁻⁷                 s/op
NanoTimeCurrentTimeMillisBenchmark.nanoTime:nanoTime·p0.95                      sample                 ≈ 10⁻⁷                 s/op
NanoTimeCurrentTimeMillisBenchmark.nanoTime:nanoTime·p0.99                      sample                 ≈ 10⁻⁷                 s/op
NanoTimeCurrentTimeMillisBenchmark.nanoTime:nanoTime·p0.999                     sample                 ≈ 10⁻⁷                 s/op
NanoTimeCurrentTimeMillisBenchmark.nanoTime:nanoTime·p0.9999                    sample                 ≈ 10⁻⁶                 s/op
NanoTimeCurrentTimeMillisBenchmark.nanoTime:nanoTime·p1.00                      sample                 ≈ 10⁻⁴                 s/op

```
